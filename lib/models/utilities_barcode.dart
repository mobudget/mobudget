import 'package:MoBudget/extensions/string.dart';
import 'package:MoBudget/models/base_barcode.dart';
import 'package:intl/intl.dart';

RegExp exp = RegExp(
    r"((?<field1>\d{11})(?<field2>\d{11})(?<field3>\d{11})(?<field4>\d{11}))");

class UtilitiesBarcode extends BaseBarcode {
  UtilitiesBarcode(String barcode) : super(barcode, exp.firstMatch(barcode));

  @override
  double value() => barcode.substring(4, 15).toMoney();

  @override
  String dueDate() {
    final String possibleDate = barcode.substring(23, 31);
    final currentYear = DateTime.now().year.toString();

    DateTime dueDate = possibleDate.startsWith(currentYear)
        ? DateTime.tryParse('${possibleDate}T03Z') ?? DateTime.now()
        : DateTime.now();
    return DateFormat("dd/MM/yyyy", Intl.getCurrentLocale()).format(dueDate);
  }

  @override
  String readableBarcode() {
    final String field1 = match.namedGroup('field1');
    final String field2 = match.namedGroup('field2');
    final String field3 = match.namedGroup('field3');
    final String field4 = match.namedGroup('field4');
    final List<String> fields = [field1, field2, field3, field4];

    final List<int> dacs = _isUtilitiesMod10()
        ? calculateDacsMod10(fields)
        : calculateDacsMod11(fields);

    return '$field1${dacs[0]}$field2${dacs[1]}$field3${dacs[2]}$field4${dacs[3]}';
  }

  List<int> calculateDacsMod10(List<String> fields) =>
      fields.map((String field) => calculateDacMod10(field, 2, 1)).toList();

  List<int> calculateDacsMod11(List<String> fields) =>
      fields.map((String field) => _calculateUtilitiesDacMod11(field)).toList();

  bool _isUtilitiesMod10() => ["6", "7"].contains(barcode[2]);

  int _calculateUtilitiesDacMod11(String numbers) {
    const int base = 11;
    List<int> multipliers = [4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];
    List<int> digits = splitNumbers(numbers);
    int total = 0;

    digits.asMap().forEach((index, digit) {
      int factor = multipliers[index];
      total += (digit * factor);
    });

    int rest = (total % base);
    switch (rest) {
      case 0:
      case 1:
        return 0;
      case 10:
        return 1;
      default:
        return (base - rest);
    }
  }
}
