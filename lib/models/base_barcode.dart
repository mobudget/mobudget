import 'package:MoBudget/models/bank_barcode.dart';
import 'package:MoBudget/models/utilities_barcode.dart';

abstract class BaseBarcode {
  BaseBarcode(this.barcode, this.match);

  final String barcode;
  final RegExpMatch match;

  static bool isUtitilies(String barcode) => barcode.startsWith('8');

  static BaseBarcode build(String barcode) {
    if (isUtitilies(barcode)) {
      return UtilitiesBarcode(barcode);
    } else {
      return BankBarcode(barcode);
    }
  }

  double value();
  String dueDate();
  String readableBarcode();

  int calculateDacMod10(String numbers, int multiplierX, int multiplierY) {
    const int base = 10;
    List<int> multipliers = [multiplierX, multiplierY];
    List<int> digits = splitNumbers(numbers);
    int total = 0;

    digits.asMap().forEach((index, digit) {
      int factor = multipliers[(index % 2)];
      int result = (digit * factor);
      if (result > 9) result = _sumDigits(result);
      total += result;
    });

    return (base - (total % base)) % base;
  }

  List<int> splitNumbers(String numbers) {
    return numbers.split('').map((String n) => int.tryParse(n)).toList();
  }

  int _sumDigits(int number) {
    return splitNumbers('$number').reduce((value, element) => value + element);
  }
}
