import 'package:basic_utils/basic_utils.dart';
import 'package:MoBudget/extensions/double.dart';
import 'package:MoBudget/extensions/string.dart';
import 'package:MoBudget/models/base_barcode.dart';
import 'package:intl/intl.dart';

RegExp exp = RegExp(
    r"(?<bank>\d{3})(?<currency>\d{1})(?<verificator>\d{1})(?<dueDateFactor>\d{4})(?<value>\d{10})(?<freeField1>\d{5})(?<freeField2>\d{10})(?<freeField3>\d{10})");

DateTime baseDate =
    (DateTime.now().compareTo(DateTime.parse('2025-02-22T03Z')) >= 0)
        ? DateTime.parse('2025-02-22T03Z')
        : DateTime.parse('1997-10-07T03Z');

class BankBarcode extends BaseBarcode {
  BankBarcode(String barcode) : super(barcode, exp.firstMatch(barcode));

  @override
  double value() => match.namedGroup('value').toMoney();

  @override
  String dueDate() {
    DateTime dueDate = baseDate.add(
      Duration(days: match.namedGroup('dueDateFactor').toInteger()),
    );
    return DateFormat("dd/MM/yyyy", Intl.getCurrentLocale()).format(dueDate);
  }

  @override
  String readableBarcode() {
    final String field1 = _calculateField1();
    final String field2 = _calculateField2();
    final String field3 = _calculateField3();

    final String verificator = match.namedGroup('verificator');
    final String dueDateFactor = match.namedGroup('dueDateFactor');
    final String value = match.namedGroup('value');

    return '$field1$field2$field3$verificator$dueDateFactor$value';
  }

  String _calculateField1() {
    final String bank = match.namedGroup('bank');
    final String currency = match.namedGroup('currency');
    final String freeField1 = match.namedGroup('freeField1');
    final int dac = calculateDacMod10('$bank$currency$freeField1', 2, 1);

    return '$bank$currency$freeField1$dac';
  }

  String _calculateField2() {
    final String freeField2 = match.namedGroup('freeField2');
    final int dac = calculateDacMod10('$freeField2', 1, 2);

    return '$freeField2$dac';
  }

  String _calculateField3() {
    final String freeField3 = match.namedGroup('freeField3');
    final int dac = calculateDacMod10('$freeField3', 1, 2);

    return '$freeField3$dac';
  }
}
