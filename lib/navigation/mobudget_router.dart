import 'package:MoBudget/screens/balance/new_screen.dart';
import 'package:MoBudget/screens/barcode_scan/screen.dart';
import 'package:MoBudget/screens/home/screen.dart';
import 'package:MoBudget/screens/record/new_screen.dart';
import 'package:MoBudget/screens/scan_page.dart';
import 'package:MoBudget/screens/sign_in_demo.dart';
import 'package:flutter/widgets.dart';
import 'package:MoBudget/screens/balance/screen.dart';
import 'package:nuvigator/nuvigator.dart';

part 'mobudget_router.g.dart';

@NuRouter()
class MoBudgetRouter extends Router {
  @NuRoute()
  ScreenRoute homeRoute() => ScreenRoute(
        builder: (_) => HomeScreen(),
      );

  @NuRoute()
  ScreenRoute balanceRoute() => ScreenRoute(
        builder: (_) => BalanceScreen(),
      );

  @NuRoute()
  ScreenRoute newRecordRoute({String recordTypeLabel}) => ScreenRoute(
        builder: (_) => NewRecordScreen(recordTypeLabel: recordTypeLabel),
      );

  @NuRoute()
  ScreenRoute newBalanceRoute() => ScreenRoute(
        builder: (_) => NewBalanceScreen(),
      );

  @NuRoute()
  ScreenRoute barcodeScanPage() => ScreenRoute(
        builder: (_) => BarcodeScanScreen(),
      );

  @NuRoute()
  ScreenRoute scanPage() => ScreenRoute(
        builder: (_) => MyHomePage(),
      );

  @NuRoute()
  ScreenRoute signInDemoPage() => ScreenRoute(
        builder: (_) => SignInDemo(),
      );

  @override
  Map<RouteDef, ScreenRouteBuilder> get screensMap => _$screensMap;
}
