// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mobudget_router.dart';

// **************************************************************************
// NuvigatorGenerator
// **************************************************************************

class MoBudgetRoutes {
  static const homeRoute = 'moBudget/homeRoute';

  static const balanceRoute = 'moBudget/balanceRoute';

  static const newRecordRoute = 'moBudget/newRecordRoute';

  static const newBalanceRoute = 'moBudget/newBalanceRoute';

  static const barcodeScanPage = 'moBudget/barcodeScanPage';

  static const scanPage = 'moBudget/scanPage';

  static const signInDemoPage = 'moBudget/signInDemoPage';
}

class NewRecordRouteArgs {
  NewRecordRouteArgs({@required this.recordTypeLabel});

  final String recordTypeLabel;

  static NewRecordRouteArgs parse(Map<String, Object> args) {
    if (args == null) {
      return NewRecordRouteArgs(recordTypeLabel: null);
    }
    return NewRecordRouteArgs(
      recordTypeLabel: args['recordTypeLabel'],
    );
  }

  Map<String, Object> get toMap => {
        'recordTypeLabel': recordTypeLabel,
      };
  static NewRecordRouteArgs of(BuildContext context) {
    final routeSettings = ModalRoute.of(context)?.settings;
    final nuvigator = Nuvigator.of(context);
    if (routeSettings?.name == MoBudgetRoutes.newRecordRoute) {
      final args = routeSettings?.arguments;
      if (args == null)
        throw FlutterError('NewRecordRouteArgs requires Route arguments');
      if (args is NewRecordRouteArgs) return args;
      if (args is Map<String, Object>) return parse(args);
    } else if (nuvigator != null) {
      return of(nuvigator.context);
    }
    return null;
  }
}

extension MoBudgetRouterNavigation on MoBudgetRouter {
  Future<Object> toHomeRoute() {
    return nuvigator.pushNamed<Object>(
      MoBudgetRoutes.homeRoute,
    );
  }

  Future<Object> pushReplacementToHomeRoute<TO extends Object>({TO result}) {
    return nuvigator.pushReplacementNamed<Object, TO>(
      MoBudgetRoutes.homeRoute,
      result: result,
    );
  }

  Future<Object> pushAndRemoveUntilToHomeRoute<TO extends Object>(
      {@required RoutePredicate predicate}) {
    return nuvigator.pushNamedAndRemoveUntil<Object>(
      MoBudgetRoutes.homeRoute,
      predicate,
    );
  }

  Future<Object> popAndPushToHomeRoute<TO extends Object>({TO result}) {
    return nuvigator.popAndPushNamed<Object, TO>(
      MoBudgetRoutes.homeRoute,
      result: result,
    );
  }

  Future<Object> toBalanceRoute() {
    return nuvigator.pushNamed<Object>(
      MoBudgetRoutes.balanceRoute,
    );
  }

  Future<Object> pushReplacementToBalanceRoute<TO extends Object>({TO result}) {
    return nuvigator.pushReplacementNamed<Object, TO>(
      MoBudgetRoutes.balanceRoute,
      result: result,
    );
  }

  Future<Object> pushAndRemoveUntilToBalanceRoute<TO extends Object>(
      {@required RoutePredicate predicate}) {
    return nuvigator.pushNamedAndRemoveUntil<Object>(
      MoBudgetRoutes.balanceRoute,
      predicate,
    );
  }

  Future<Object> popAndPushToBalanceRoute<TO extends Object>({TO result}) {
    return nuvigator.popAndPushNamed<Object, TO>(
      MoBudgetRoutes.balanceRoute,
      result: result,
    );
  }

  Future<Object> toNewRecordRoute({String recordTypeLabel}) {
    return nuvigator.pushNamed<Object>(
      MoBudgetRoutes.newRecordRoute,
      arguments: {
        'recordTypeLabel': recordTypeLabel,
      },
    );
  }

  Future<Object> pushReplacementToNewRecordRoute<TO extends Object>(
      {String recordTypeLabel, TO result}) {
    return nuvigator.pushReplacementNamed<Object, TO>(
      MoBudgetRoutes.newRecordRoute,
      arguments: {
        'recordTypeLabel': recordTypeLabel,
      },
      result: result,
    );
  }

  Future<Object> pushAndRemoveUntilToNewRecordRoute<TO extends Object>(
      {String recordTypeLabel, @required RoutePredicate predicate}) {
    return nuvigator.pushNamedAndRemoveUntil<Object>(
      MoBudgetRoutes.newRecordRoute,
      predicate,
      arguments: {
        'recordTypeLabel': recordTypeLabel,
      },
    );
  }

  Future<Object> popAndPushToNewRecordRoute<TO extends Object>(
      {String recordTypeLabel, TO result}) {
    return nuvigator.popAndPushNamed<Object, TO>(
      MoBudgetRoutes.newRecordRoute,
      arguments: {
        'recordTypeLabel': recordTypeLabel,
      },
      result: result,
    );
  }

  Future<Object> toNewBalanceRoute() {
    return nuvigator.pushNamed<Object>(
      MoBudgetRoutes.newBalanceRoute,
    );
  }

  Future<Object> pushReplacementToNewBalanceRoute<TO extends Object>(
      {TO result}) {
    return nuvigator.pushReplacementNamed<Object, TO>(
      MoBudgetRoutes.newBalanceRoute,
      result: result,
    );
  }

  Future<Object> pushAndRemoveUntilToNewBalanceRoute<TO extends Object>(
      {@required RoutePredicate predicate}) {
    return nuvigator.pushNamedAndRemoveUntil<Object>(
      MoBudgetRoutes.newBalanceRoute,
      predicate,
    );
  }

  Future<Object> popAndPushToNewBalanceRoute<TO extends Object>({TO result}) {
    return nuvigator.popAndPushNamed<Object, TO>(
      MoBudgetRoutes.newBalanceRoute,
      result: result,
    );
  }

  Future<Object> toBarcodeScanPage() {
    return nuvigator.pushNamed<Object>(
      MoBudgetRoutes.barcodeScanPage,
    );
  }

  Future<Object> pushReplacementToBarcodeScanPage<TO extends Object>(
      {TO result}) {
    return nuvigator.pushReplacementNamed<Object, TO>(
      MoBudgetRoutes.barcodeScanPage,
      result: result,
    );
  }

  Future<Object> pushAndRemoveUntilToBarcodeScanPage<TO extends Object>(
      {@required RoutePredicate predicate}) {
    return nuvigator.pushNamedAndRemoveUntil<Object>(
      MoBudgetRoutes.barcodeScanPage,
      predicate,
    );
  }

  Future<Object> popAndPushToBarcodeScanPage<TO extends Object>({TO result}) {
    return nuvigator.popAndPushNamed<Object, TO>(
      MoBudgetRoutes.barcodeScanPage,
      result: result,
    );
  }

  Future<Object> toScanPage() {
    return nuvigator.pushNamed<Object>(
      MoBudgetRoutes.scanPage,
    );
  }

  Future<Object> pushReplacementToScanPage<TO extends Object>({TO result}) {
    return nuvigator.pushReplacementNamed<Object, TO>(
      MoBudgetRoutes.scanPage,
      result: result,
    );
  }

  Future<Object> pushAndRemoveUntilToScanPage<TO extends Object>(
      {@required RoutePredicate predicate}) {
    return nuvigator.pushNamedAndRemoveUntil<Object>(
      MoBudgetRoutes.scanPage,
      predicate,
    );
  }

  Future<Object> popAndPushToScanPage<TO extends Object>({TO result}) {
    return nuvigator.popAndPushNamed<Object, TO>(
      MoBudgetRoutes.scanPage,
      result: result,
    );
  }

  Future<Object> toSignInDemoPage() {
    return nuvigator.pushNamed<Object>(
      MoBudgetRoutes.signInDemoPage,
    );
  }

  Future<Object> pushReplacementToSignInDemoPage<TO extends Object>(
      {TO result}) {
    return nuvigator.pushReplacementNamed<Object, TO>(
      MoBudgetRoutes.signInDemoPage,
      result: result,
    );
  }

  Future<Object> pushAndRemoveUntilToSignInDemoPage<TO extends Object>(
      {@required RoutePredicate predicate}) {
    return nuvigator.pushNamedAndRemoveUntil<Object>(
      MoBudgetRoutes.signInDemoPage,
      predicate,
    );
  }

  Future<Object> popAndPushToSignInDemoPage<TO extends Object>({TO result}) {
    return nuvigator.popAndPushNamed<Object, TO>(
      MoBudgetRoutes.signInDemoPage,
      result: result,
    );
  }
}

extension MoBudgetRouterScreensAndRouters on MoBudgetRouter {
  Map<RouteDef, ScreenRouteBuilder> get _$screensMap {
    return {
      RouteDef(MoBudgetRoutes.homeRoute): (RouteSettings settings) {
        return homeRoute();
      },
      RouteDef(MoBudgetRoutes.balanceRoute): (RouteSettings settings) {
        return balanceRoute();
      },
      RouteDef(MoBudgetRoutes.newRecordRoute): (RouteSettings settings) {
        final args = NewRecordRouteArgs.parse(settings.arguments);
        return newRecordRoute(recordTypeLabel: args.recordTypeLabel);
      },
      RouteDef(MoBudgetRoutes.newBalanceRoute): (RouteSettings settings) {
        return newBalanceRoute();
      },
      RouteDef(MoBudgetRoutes.barcodeScanPage): (RouteSettings settings) {
        return barcodeScanPage();
      },
      RouteDef(MoBudgetRoutes.scanPage): (RouteSettings settings) {
        return scanPage();
      },
      RouteDef(MoBudgetRoutes.signInDemoPage): (RouteSettings settings) {
        return signInDemoPage();
      },
    };
  }
}
