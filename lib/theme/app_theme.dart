import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppTheme {
  AppTheme({@required this.context});

  BuildContext context;

  ThemeData get themeData {
    return ThemeData(
      primarySwatch: Colors.indigo,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      scaffoldBackgroundColor: Colors.indigo[400],
      primaryColor: Colors.indigo[400],
      accentColor: Colors.indigo[300],
      dividerColor: Colors.indigo[300],
      iconTheme: IconThemeData(color: Colors.white),
      tooltipTheme: TooltipThemeData(
        decoration: BoxDecoration(
          color: Colors.indigo[200],
        ),
      ),
      buttonTheme: ButtonThemeData(buttonColor: Colors.indigoAccent),
      fontFamily: 'Poppins',
      textTheme: GoogleFonts.poppinsTextTheme(
        Theme.of(context).textTheme.copyWith(
              bodyText1: TextStyle(color: Colors.indigo[400]),
              bodyText2: TextStyle(color: Colors.white),
              subtitle1: TextStyle(color: Colors.indigo[400]),
              subtitle2: TextStyle(color: Colors.white),
            ),
      ),
    );
  }
}

extension CustomColorScheme on ColorScheme {
  Color get positiveColor => Colors.green[500];
  Color get negativeColor => Colors.red[500];
  Color get investmentColor => Colors.blue[500];

  Color get confirmedColor => Colors.green[500];
  Color get scheduledColor => Colors.orange[500];
  Color get pendingColor => Colors.red[500];

  Color get revenuesBarColor => Colors.green[500];
  Color get investmentsBarColor => Colors.orange[500];
  Color get expensesBarColor => Colors.red[500];

  Color get flushbarIconColor => Colors.indigoAccent;
  Color get flushbarIndicatorColor => Colors.indigo;

  Color get situationChipColor => Colors.white;
  Color get chartSecondaryTextColor => Colors.white54;
  Color get tooltipBgColor => Colors.indigo[50];
  Color get overlayColor => Colors.indigo[50];
}
