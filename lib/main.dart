import 'package:MoBudget/navigation/mobudget_router.dart';
import 'package:MoBudget/screens/balance/screen.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart';
import 'package:nuvigator/nuvigator.dart';

import 'theme/app_theme.dart';

void main() {
  Intl.defaultLocale = 'pt_BR';

  LicenseRegistry.addLicense(() async* {
    final license = await rootBundle.loadString('google_fonts/OFL.txt');
    yield LicenseEntryWithLineBreaks(['google_fonts'], license);
  });

  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then(
    (_) => runApp(MoBubgetApp()),
  );
}

class MoBubgetApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [const Locale('pt', 'BR')],
      title: 'MoBudget',
      theme: AppTheme(context: context).themeData,
      home: BalanceScreen(),
      builder: Nuvigator(
        router: MoBudgetRouter(),
        screenType: materialScreenType,
        initialRoute: MoBudgetRoutes.signInDemoPage,
      ),
    );
  }
}
