import 'package:MoBudget/navigation/mobudget_router.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nuvigator/nuvigator.dart';

import 'package:url_launcher/url_launcher.dart';

import 'sheet_item.dart';

class HomeBottomSheet extends StatelessWidget {
  const HomeBottomSheet({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 32.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                HomeSheetItem(
                  onTap: () {
                    Router.of<MoBudgetRouter>(context).toBalanceRoute();
                  },
                  icon: Icons.assessment,
                  label: '(Atual) Setembro de 2020',
                  padding: const EdgeInsets.symmetric(
                    vertical: 8.0,
                    horizontal: 2.0,
                  ),
                ),
                HomeSheetItem(
                  onTap: () {
                    Router.of<MoBudgetRouter>(context).toScanPage();
                  },
                  icon: Icons.assessment,
                  label: 'Agosto de 2020',
                  padding: const EdgeInsets.symmetric(
                    vertical: 8.0,
                    horizontal: 2.0,
                  ),
                ),
                HomeSheetItem(
                  onTap: () {},
                  icon: Icons.assessment,
                  label: 'Julho de 2020',
                  padding: const EdgeInsets.symmetric(
                    vertical: 8.0,
                    horizontal: 2.0,
                  ),
                ),
                HomeSheetItem(
                  onTap: () {},
                  icon: Icons.assessment,
                  label: 'Junho de 2020',
                  padding: const EdgeInsets.symmetric(
                    vertical: 8.0,
                    horizontal: 2.0,
                  ),
                ),
                HomeSheetItem(
                  onTap: () {},
                  icon: Icons.assessment,
                  label: 'Maio de 2020',
                  padding: const EdgeInsets.symmetric(
                    vertical: 8.0,
                    horizontal: 2.0,
                  ),
                ),
                HomeSheetItem(
                  onTap: () {},
                  icon: Icons.assessment,
                  label: 'Abril de 2020',
                  padding: const EdgeInsets.symmetric(
                    vertical: 8.0,
                    horizontal: 2.0,
                  ),
                ),
                HomeSheetItem(
                  onTap: () {},
                  icon: Icons.assessment,
                  label: 'Março de 2020',
                  padding: const EdgeInsets.symmetric(
                    vertical: 8.0,
                    horizontal: 2.0,
                  ),
                ),
                HomeSheetItem(
                  onTap: () {},
                  icon: Icons.assessment,
                  label: 'Fevereiro de 2020',
                  padding: const EdgeInsets.symmetric(
                    vertical: 8.0,
                    horizontal: 2.0,
                  ),
                ),
                HomeSheetItem(
                  onTap: () {},
                  icon: Icons.assessment,
                  label: 'Janeiro de 2020',
                  padding: const EdgeInsets.symmetric(
                    vertical: 8.0,
                    horizontal: 2.0,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
