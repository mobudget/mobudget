import 'package:MoBudget/components/indicator.dart';
import 'package:MoBudget/theme/app_theme.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:MoBudget/extensions/double.dart';

class BarChartSample2 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => BarChartSample2State();
}

class BarChartSample2State extends State<BarChartSample2> {
  final Color revenuesBarColor = Colors.green[500];
  final Color investmentsBarColor = Colors.orange[500];
  final Color expensesBarColor = Colors.red[500];

  final double width = 7;
  double maxValue;

  List<BarChartGroupData> rawBarGroups;
  List<BarChartGroupData> showingBarGroups;

  int touchedGroupIndex;

  @override
  void initState() {
    super.initState();
    final barGroup1 = _makeGroupData(0, 10000, 2000, 8000);
    final barGroup2 = _makeGroupData(1, 11000, 3000, 8000);
    final barGroup3 = _makeGroupData(2, 9000, 1000, 7000);
    final barGroup4 = _makeGroupData(3, 10000, 3000, 7000);
    final barGroup5 = _makeGroupData(4, 12000, 5000, 7000);
    final barGroup6 = _makeGroupData(5, 10000, 1000, 9000);

    maxValue = 12000.0; // Use multiples of 400, 4000, 40000

    final items = [
      barGroup1,
      barGroup2,
      barGroup3,
      barGroup4,
      barGroup5,
      barGroup6,
    ];

    rawBarGroups = items;

    showingBarGroups = rawBarGroups;
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final ColorScheme colorScheme = theme.colorScheme;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: AspectRatio(
        aspectRatio: 1.4,
        child: Card(
          elevation: 4,
          color: theme.accentColor,
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    _makeTransactionsIcon(),
                    const SizedBox(width: 16),
                    const Text(
                      'Resumo',
                      style: TextStyle(fontSize: 22),
                    ),
                    Text(
                      ' dos últimos 6 meses',
                      style: TextStyle(
                        color: colorScheme.chartSecondaryTextColor,
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 16),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Indicator(
                      color: revenuesBarColor,
                      text: 'Receitas',
                    ),
                    Indicator(
                      color: investmentsBarColor,
                      text: 'Investimentos',
                    ),
                    Indicator(
                      color: expensesBarColor,
                      text: 'Despesas',
                    ),
                  ],
                ),
                const SizedBox(height: 24),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: BarChart(
                      BarChartData(
                        maxY: maxValue,
                        barTouchData: BarTouchData(
                          touchTooltipData: BarTouchTooltipData(
                            tooltipBgColor: colorScheme.tooltipBgColor,
                            getTooltipItem: _getTooltipItem,
                          ),
                        ),
                        titlesData: FlTitlesData(
                          show: true,
                          bottomTitles: SideTitles(
                            showTitles: true,
                            textStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 10,
                            ),
                            margin: 8,
                            getTitles: _getXTitles,
                          ),
                          leftTitles: SideTitles(
                            showTitles: true,
                            textStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 10,
                            ),
                            margin: 32,
                            reservedSize: 48,
                            getTitles: getYTitles,
                          ),
                        ),
                        borderData: FlBorderData(show: false),
                        barGroups: showingBarGroups,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  BarTooltipItem _getTooltipItem(group, groupIndex, rod, rodIndex) {
    String recordType;
    switch (rodIndex) {
      case 0:
        recordType = 'Receitas';
        break;
      case 1:
        recordType = 'Investimentos';
        break;
      case 2:
        recordType = 'Despesas';
        break;
    }

    String month;
    switch (groupIndex) {
      case 0:
        month = 'Abril';
        break;
      case 1:
        month = 'Maio';
        break;
      case 2:
        month = 'Junho';
        break;
      case 3:
        month = 'Julho';
        break;
      case 4:
        month = 'Agosto';
        break;
      case 5:
        month = 'Setembro';
        break;
    }

    final formattedAmount = _formattedAmount(rod.y);

    return BarTooltipItem(
      '$month\n$recordType\n$formattedAmount',
      TextStyle(
        color: Theme.of(context).primaryColor,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  String getYTitles(double value) {
    if ((value % (maxValue / 4).ceil()) == 0) {
      return _formattedAmount(value);
    } else {
      return '';
    }
  }

  String _formattedAmount(double value) => value.toBRL();

  String _getXTitles(double value) {
    switch (value.toInt()) {
      case 0:
        return 'Abr';
      case 1:
        return 'Mai';
      case 2:
        return 'Jun';
      case 3:
        return 'Jul';
      case 4:
        return 'Ago';
      case 5:
        return 'Set';
      default:
        return '';
    }
  }

  BarChartGroupData _makeGroupData(int x, double y1, double y2, double y3) {
    return BarChartGroupData(
      barsSpace: 4,
      x: x,
      barRods: [
        BarChartRodData(
          y: y1,
          color: revenuesBarColor,
          width: width,
        ),
        BarChartRodData(
          y: y2,
          color: investmentsBarColor,
          width: width,
        ),
        BarChartRodData(
          y: y3,
          color: expensesBarColor,
          width: width,
        ),
      ],
    );
  }

  Widget _makeTransactionsIcon() {
    const double width = 4.5;
    const double space = 3.5;
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          width: width,
          height: 10,
          color: Colors.white.withOpacity(0.4),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 28,
          color: Colors.white.withOpacity(0.8),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 42,
          color: Colors.white.withOpacity(1),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 28,
          color: Colors.white.withOpacity(0.8),
        ),
        const SizedBox(
          width: space,
        ),
        Container(
          width: width,
          height: 10,
          color: Colors.white.withOpacity(0.4),
        ),
      ],
    );
  }
}
