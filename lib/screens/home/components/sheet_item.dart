import 'package:flutter/material.dart';

class HomeSheetItem extends StatelessWidget {
  const HomeSheetItem({
    Key key,
    @required this.icon,
    @required this.label,
    @required this.onTap,
    this.height = 100,
    this.width = 90,
    this.padding = const EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
  }) : super(key: key);

  final IconData icon;
  final String label;
  final VoidCallback onTap;
  final double height;
  final double width;
  final EdgeInsets padding;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
        elevation: 4.0,
        color: Theme.of(context).accentColor,
        child: InkWell(
          onTap: onTap,
          child: Container(
            height: height,
            width: width,
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Icon(icon, size: 24.0),
                Flexible(
                  child: Text(
                    label,
                    maxLines: 3,
                    softWrap: true,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 11.0,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
