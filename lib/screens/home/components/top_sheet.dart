import 'package:MoBudget/navigation/mobudget_router.dart';
import 'package:flutter/material.dart';
import 'package:nuvigator/nuvigator.dart';

import 'sheet_item.dart';

class HomeTopSheet extends StatelessWidget {
  const HomeTopSheet({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                HomeSheetItem(
                  onTap: () => Router.of<MoBudgetRouter>(context)
                      .toNewRecordRoute(recordTypeLabel: 'Receita'),
                  icon: Icons.attach_money,
                  label: 'Adicionar receita',
                ),
                HomeSheetItem(
                  onTap: () => Router.of<MoBudgetRouter>(context)
                      .toNewRecordRoute(recordTypeLabel: 'Investimento'),
                  icon: Icons.trending_up,
                  label: 'Adicionar investimento',
                ),
                HomeSheetItem(
                  onTap: () => Router.of<MoBudgetRouter>(context)
                      .toNewRecordRoute(recordTypeLabel: 'Despesa'),
                  icon: Icons.money_off,
                  label: 'Adicionar despesa',
                ),
                HomeSheetItem(
                  onTap: () =>
                      Router.of<MoBudgetRouter>(context).toNewBalanceRoute(),
                  icon: Icons.library_add,
                  label: 'Adicionar balanço',
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
