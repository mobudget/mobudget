import 'package:MoBudget/components/screen.dart';
import 'package:flutter/material.dart';

import 'components/bar_chart_sample2.dart';
import 'components/bottom_sheet.dart';
import 'components/pie_chart_sample2.dart';
import 'components/top_sheet.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MoBudgetScreen(
      topSheet: PreferredSize(
        preferredSize: Size(double.maxFinite, 100),
        child: HomeTopSheet(),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          BarChartSample2(),
          PieChartSample2(),
        ],
      ),
      bottomSheet: HomeBottomSheet(),
    );
  }
}
