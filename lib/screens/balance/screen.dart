import 'package:MoBudget/components/screen.dart';
import 'package:MoBudget/screens/record/components/menu.dart';
import 'package:flutter/material.dart';

import 'components/investments_card.dart';
import 'components/resume_card.dart';
import 'components/expenses_card.dart';
import 'components/reveneus_card.dart';

class BalanceScreen extends StatefulWidget {
  @override
  _BalanceScreenState createState() => _BalanceScreenState();
}

class _BalanceScreenState extends State<BalanceScreen> {
  @override
  Widget build(BuildContext context) {
    return MoBudgetScreen(
      floatingActionButton: RecordMenu(),
      body: Column(
        children: [
          ResumeCard(),
          ReveneusCard(),
          InvestmentsCard(),
          Padding(
            padding: const EdgeInsets.only(bottom: 80.0),
            child: ExpensesCard(),
          ),
        ],
      ),
    );
  }
}
