import 'package:MoBudget/components/screen.dart';
import 'package:MoBudget/screens/balance/components/form.dart';
import 'package:flutter/material.dart';

class NewBalanceScreen extends StatefulWidget {
  const NewBalanceScreen({
    Key key,
  }) : super(key: key);

  @override
  _NewBalanceScreenState createState() => _NewBalanceScreenState();
}

class _NewBalanceScreenState extends State<NewBalanceScreen> {
  @override
  Widget build(BuildContext context) {
    return MoBudgetScreen(
      title: 'Novo balanço mensal',
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: BalanceForm(),
          ),
        ),
      ),
    );
  }
}
