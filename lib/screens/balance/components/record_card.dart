import 'package:MoBudget/components/expandable_card.dart';
import 'package:MoBudget/theme/app_theme.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:MoBudget/extensions/double.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

class RecordCard extends StatelessWidget {
  RecordCard(
    this.situationColor,
    this.date,
    this.record,
    this.value,
    this.description,
    this.category,
    this.situation, {
    this.barcode,
  });

  final Color situationColor;
  final String date;
  final String record;
  final double value;
  final String description;
  final String category;
  final String situation;
  final String barcode;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final ColorScheme colorScheme = theme.colorScheme;
    final String formatedValue = value.toBRL();

    return ExpandableCard(
      hasIcon: false,
      header: ListTile(
        visualDensity: VisualDensity(vertical: -4),
        leading: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(
              Icons.lens,
              color: situationColor,
              size: 16.0,
            ),
            Text(
              ' $date',
              style: Theme.of(context).textTheme.overline.copyWith(
                    fontSize: 10.0,
                  ),
            ),
          ],
        ),
        title: Text(
          record,
          softWrap: true,
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          style: Theme.of(context).textTheme.overline.copyWith(
                fontSize: 12.0,
              ),
        ),
        subtitle: Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Text(formatedValue),
        ),
        trailing: PopupMenuButton<int>(
          icon: Icon(Icons.more_vert, color: theme.primaryColor),
          color: Theme.of(context).accentColor,
          tooltip: 'Opções',
          offset: Offset(0, 100),
          onSelected: (int value) {
            if (value == 3) {
              Clipboard.setData(ClipboardData(text: barcode)).then(
                (_) async {
                  await launch(
                    'https://nuapp.nubank.com.br/bnVhcHA6Ly9zYXZpbmdz',
                    forceSafariVC: false,
                    universalLinksOnly: true,
                  );
                },
              );
            } else {
              Flushbar(
                icon: Icon(
                  Icons.check_circle,
                  size: 28.0,
                  color: colorScheme.flushbarIconColor,
                ),
                message: 'Ação executada!',
                duration: Duration(seconds: 3),
                leftBarIndicatorColor: colorScheme.flushbarIndicatorColor,
                dismissDirection: FlushbarDismissDirection.HORIZONTAL,
              )..show(context);
            }
          },
          itemBuilder: (context) => [
            PopupMenuItem(
              textStyle: theme.textTheme.subtitle2,
              value: 1,
              child: Text("Editar"),
            ),
            PopupMenuItem(
              value: 2,
              textStyle: theme.textTheme.subtitle2,
              child: Text("Remover"),
            ),
            PopupMenuItem(
              enabled: barcode != null,
              value: 3,
              textStyle: theme.textTheme.subtitle2,
              child: Text("Pagar boleto com Nubank"),
            ),
            PopupMenuItem(
              value: 3,
              textStyle: theme.textTheme.subtitle2,
              child: Text("Já Paguei!"),
            ),
            PopupMenuItem(
              value: 3,
              textStyle: theme.textTheme.subtitle2,
              child: Text("Já Agendei!"),
            ),
          ],
        ),
      ),
      expanded: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Column(
          children: ListTile.divideTiles(context: context, tiles: [
            ListTile(
              title: Text(
                record,
                softWrap: true,
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              dense: true,
              title: Text(
                'Descrição',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Text(
                description,
                softWrap: true,
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            ListTile(
              dense: true,
              title: Text(
                'Data',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Text(
                date,
                softWrap: true,
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            ListTile(
              dense: true,
              title: Text(
                'Valor',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Text(
                formatedValue,
                softWrap: true,
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            ListTile(
              dense: true,
              title: Text(
                'Categoria',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Text(
                category,
                softWrap: true,
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            ListTile(
              dense: true,
              title: Text(
                'Situação',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Text(
                situation,
                softWrap: true,
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ]).toList(),
        ),
      ),
    );
  }
}
