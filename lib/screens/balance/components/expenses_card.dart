import 'package:MoBudget/components/expandable_card.dart';
import 'package:MoBudget/theme/app_theme.dart';
import 'package:flutter/material.dart';

import 'record_card.dart';
import 'situation_chip.dart';

class ExpensesCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final ColorScheme colorScheme = theme.colorScheme;

    final _expandableCard = RecordCard(
      colorScheme.pendingColor,
      '13/09/2020',
      'Cartão Nubank',
      100.0,
      'Conta do cartão Nubank',
      'Cartão de crédito',
      'Pendente',
    );
    final _expandableCard2 = RecordCard(
      colorScheme.scheduledColor,
      '12/09/2020',
      'Cartão C6',
      50.0,
      'Conta do C6 Bank',
      'Cartão de crédito',
      'Agendada',
    );
    final _expandableCard3 = RecordCard(
      colorScheme.confirmedColor,
      '10/09/2020',
      'Aluguel',
      400.0,
      'Aluguel do apartamento',
      'Casa',
      'Confirmada',
    );
    final _expandableCard4 = RecordCard(
      colorScheme.confirmedColor,
      '09/09/2020',
      'Gás',
      50.0,
      'Conta da Compresa',
      'Casa',
      'Confirmada',
      barcode: '836800000009027400577147003242459000004169987403',
    );

    return ExpandableCard(
      padding: const EdgeInsets.all(8.0),
      header: ListTile(
        leading: Icon(
          Icons.money_off,
          color: theme.primaryColor,
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Despesas',
              softWrap: true,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              'R\$ 600,00',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
      collapsed: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          children: [
            SituationChip(
              count: 2,
              label: 'Confirmadas',
              color: colorScheme.confirmedColor,
            ),
            SituationChip(
              count: 1,
              label: 'Agendada',
              color: colorScheme.scheduledColor,
            ),
            SituationChip(
              count: 1,
              label: 'Pendente',
              color: colorScheme.pendingColor,
            ),
          ],
        ),
      ),
      expanded: Column(
        children: [
          _expandableCard,
          _expandableCard2,
          _expandableCard3,
          _expandableCard4,
        ],
      ),
    );
  }
}
