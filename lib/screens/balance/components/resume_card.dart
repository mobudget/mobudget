import 'package:MoBudget/components/expandable_card.dart';
import 'package:MoBudget/theme/app_theme.dart';
import 'package:flutter/material.dart';

class ResumeCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final ColorScheme colorScheme = theme.colorScheme;

    return ExpandableCard(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      header: ListTile(
        leading: Icon(
          Icons.assessment,
          color: theme.primaryColor,
        ),
        title: Text(
          'Balanço mensal - 09/2020',
          softWrap: true,
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      collapsed: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 8.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Resultado',
                  style: TextStyle(
                    color: theme.primaryColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Spacer(),
                Text(
                  'R\$ 200,00',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: colorScheme.positiveColor,
                  ),
                ),
                Text(
                  ' (20%)',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: colorScheme.positiveColor,
                  ),
                ),
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Saldo anterior',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: theme.primaryColor,
                  ),
                ),
                Text(
                  '+ R\$ 100,00',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: colorScheme.positiveColor,
                  ),
                ),
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Novo saldo',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: theme.primaryColor,
                  ),
                ),
                Text(
                  'R\$ 300,00',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: colorScheme.positiveColor,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      expanded: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Total de receitas',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: theme.primaryColor,
                  ),
                ),
                Text(
                  '+ R\$ 1.000,00',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: colorScheme.positiveColor,
                  ),
                ),
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Total de despesas',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: theme.primaryColor,
                  ),
                ),
                Spacer(),
                Text(
                  '- R\$ 600,00',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: colorScheme.negativeColor,
                  ),
                ),
                Text(
                  ' (60%)',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: colorScheme.negativeColor,
                  ),
                ),
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Total de investimentos',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: theme.primaryColor,
                  ),
                ),
                Spacer(),
                Text(
                  '- R\$ 200,00',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: colorScheme.investmentColor,
                  ),
                ),
                Text(
                  ' (20%)',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: colorScheme.investmentColor,
                  ),
                ),
              ],
            ),
            Divider(),
            Padding(
              padding: const EdgeInsets.only(top: 24.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Resultado',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: theme.primaryColor,
                    ),
                  ),
                  Spacer(),
                  Text(
                    'R\$ 200,00',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: colorScheme.positiveColor,
                    ),
                  ),
                  Text(
                    ' (20%)',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: colorScheme.positiveColor,
                    ),
                  ),
                ],
              ),
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Saldo anterior',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: theme.primaryColor,
                  ),
                ),
                Text(
                  '+ R\$ 100,00',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: colorScheme.positiveColor,
                  ),
                ),
              ],
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Novo saldo',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: theme.primaryColor,
                  ),
                ),
                Text(
                  'R\$ 300,00',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: colorScheme.positiveColor,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
