import 'package:MoBudget/components/expandable_card.dart';
import 'package:MoBudget/theme/app_theme.dart';
import 'package:flutter/material.dart';

import 'record_card.dart';
import 'situation_chip.dart';

class ReveneusCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final ColorScheme colorScheme = theme.colorScheme;

    final _expandableCard = RecordCard(
      colorScheme.pendingColor,
      '13/09/2020',
      'Aluguel',
      200.0,
      'Apartamento alugado em PE',
      'Aluguel',
      'Agendado',
    );
    final _expandableCard2 = RecordCard(
      colorScheme.confirmedColor,
      '10/09/2020',
      'Salário',
      800.0,
      'Salário recebido do Nubank',
      'Salário',
      'Confirmada',
    );

    return ExpandableCard(
      padding: const EdgeInsets.all(8.0),
      header: ListTile(
        leading: Icon(
          Icons.attach_money,
          color: theme.primaryColor,
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Receitas',
              softWrap: true,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              'R\$ 1.000,00',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
      collapsed: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          children: [
            SituationChip(
              count: 1,
              label: 'Confirmada',
              color: colorScheme.confirmedColor,
            ),
            SituationChip(
              count: 1,
              label: 'Agendada',
              color: colorScheme.scheduledColor,
            ),
            SituationChip(
              count: 0,
              label: 'Pendentes',
              color: colorScheme.pendingColor,
            ),
          ],
        ),
      ),
      expanded: Column(
        children: [
          _expandableCard,
          _expandableCard2,
        ],
      ),
    );
  }
}
