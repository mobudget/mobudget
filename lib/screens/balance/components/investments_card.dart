import 'package:MoBudget/components/expandable_card.dart';
import 'package:MoBudget/theme/app_theme.dart';
import 'package:flutter/material.dart';

import 'record_card.dart';
import 'situation_chip.dart';

class InvestmentsCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final ColorScheme colorScheme = theme.colorScheme;

    final _expandableCard = RecordCard(
      colorScheme.scheduledColor,
      '13/09/2020',
      'Previdência Privada',
      100.0,
      'Investimento em previdência privada na Vitreo',
      'Previdência privada',
      'Agendado',
    );
    final _expandableCard2 = RecordCard(
      colorScheme.confirmedColor,
      '10/09/2020',
      'Reserva de emergência',
      50.0,
      'Depósito em CDB para reserva de emergência',
      'Reserva de emergência',
      'Confirmada',
    );
    final _expandableCard3 = RecordCard(
      colorScheme.confirmedColor,
      '09/09/2020',
      'MXRF11',
      50.0,
      'Compra de Fiis da MXRF11',
      'Fii',
      'Confirmada',
    );

    return ExpandableCard(
      padding: const EdgeInsets.all(8.0),
      header: ListTile(
        leading: Icon(
          Icons.trending_up,
          color: theme.primaryColor,
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Investimentos',
              softWrap: true,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              'R\$ 200,00',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
      collapsed: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        child: Row(
          children: [
            SituationChip(
              count: 2,
              label: 'Confirmados',
              color: colorScheme.confirmedColor,
            ),
            SituationChip(
              count: 1,
              label: 'Agendado',
              color: colorScheme.scheduledColor,
            ),
            SituationChip(
              count: 0,
              label: 'Pendentes',
              color: colorScheme.pendingColor,
            ),
          ],
        ),
      ),
      expanded: Column(
        children: [
          _expandableCard,
          _expandableCard2,
          _expandableCard3,
        ],
      ),
    );
  }
}
