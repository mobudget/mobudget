import 'package:MoBudget/screens/balance/components/record_check_box.dart';
import 'package:MoBudget/components/expandable_card.dart';
import 'package:MoBudget/theme/app_theme.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

import 'package:MoBudget/extensions/string.dart';

class BalanceForm extends StatefulWidget {
  const BalanceForm({
    Key key,
  }) : super(key: key);

  @override
  _BalanceFormState createState() => _BalanceFormState();
}

class _BalanceFormState extends State<BalanceForm> {
  final _formKey = GlobalKey<FormState>();

  int _month = DateTime.now().month;
  int _year = DateTime.now().year;

  Flushbar<String> flush;

  List<Map<String, dynamic>> reveneus = [
    {
      'label': 'Salário Nubank',
      'value': 200.0,
      'checked': true,
    },
    {
      'label': 'Aluguel Apartameno de Paulista',
      'value': 200.0,
      'checked': true,
    },
  ];
  List<Map<String, dynamic>> investments = [
    {
      'label': 'Previdência Privada',
      'value': 2000.0,
      'checked': true,
    },
    {
      'label': 'Reserva de Emergênci',
      'value': 500.0,
      'checked': true,
    },
  ];
  List<Map<String, dynamic>> expenses = [
    {
      'label': 'Cartão Nubank',
      'value': 500.0,
      'checked': true,
    },
    {
      'label': 'Aluguel + Condomínio',
      'value': 2500.0,
      'checked': true,
    },
  ];

  String _nullValidator(dynamic value) {
    return value == null ? 'Esse campo não pode ficar em branco' : null;
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final ColorScheme colorScheme = theme.colorScheme;

    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          DropdownButtonFormField(
            value: _month,
            decoration: InputDecoration(
              icon: FaIcon(FontAwesomeIcons.calendarDay),
              labelText: 'Mês',
            ),
            items: List<DropdownMenuItem>.generate(
              12,
              (index) => DropdownMenuItem(
                child: Text(monthName(index + 1)),
                value: index + 1,
              ),
            ),
            onChanged: (value) => setState(() => _month = value),
            validator: _nullValidator,
          ),
          DropdownButtonFormField(
            value: _year,
            decoration: InputDecoration(
              icon: FaIcon(FontAwesomeIcons.calendarWeek),
              labelText: 'Ano',
            ),
            items: List<DropdownMenuItem>.generate(
              10,
              (index) => DropdownMenuItem(
                child: Text('${DateTime.now().year + index}'),
                value: DateTime.now().year + index,
              ),
            ),
            onChanged: (value) => setState(() => _year = value),
            validator: _nullValidator,
          ),
          ExpandableCard(
            padding: EdgeInsets.only(top: 16.0),
            header: ListTile(
              leading: Icon(Icons.attach_money),
              title: Text('Receitas recorrentes'),
              subtitle: FittedBox(
                child: Text('Desmarque as que deseja remover'),
              ),
            ),
            expanded: Column(
              children: reveneus.map((reveneu) {
                return RecordCheckBox(
                  title: reveneu['label'],
                  value: reveneu['value'],
                  checked: reveneu['checked'],
                );
              }).toList(),
            ),
          ),
          ExpandableCard(
            padding: EdgeInsets.only(top: 16.0),
            header: ListTile(
              leading: Icon(Icons.trending_up),
              title: Text('Investimentos recorrentes'),
              subtitle: FittedBox(
                child: Text('Desmarque os que deseja remover'),
              ),
            ),
            expanded: Column(
              children: investments.map((reveneu) {
                return RecordCheckBox(
                  title: reveneu['label'],
                  value: reveneu['value'],
                  checked: reveneu['checked'],
                );
              }).toList(),
            ),
          ),
          ExpandableCard(
            padding: EdgeInsets.only(top: 16.0),
            header: ListTile(
              leading: Icon(Icons.money_off),
              title: Text('Despesas recorrentes'),
              subtitle: FittedBox(
                child: Text('Desmarque as que deseja remover'),
              ),
            ),
            expanded: Column(
              children: expenses.map((reveneu) {
                return RecordCheckBox(
                  title: reveneu['label'],
                  value: reveneu['value'],
                  checked: reveneu['checked'],
                );
              }).toList(),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: Container(
              margin: EdgeInsets.all(4.0),
              width: double.infinity,
              child: RaisedButton(
                child: Text('Salvar'),
                textColor: Colors.white,
                color: Theme.of(context).accentColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(4.0)),
                ),
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    Navigator.pop(context);

                    Flushbar(
                      icon: Icon(
                        Icons.check_circle,
                        size: 28.0,
                        color: colorScheme.flushbarIconColor,
                      ),
                      message: 'Salvo com sucesso!',
                      duration: Duration(seconds: 3),
                      leftBarIndicatorColor: colorScheme.flushbarIndicatorColor,
                      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
                    )..show(context);
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  String monthName(int value) => DateFormat.MMMM(Intl.getCurrentLocale())
      .format(DateTime(2020, value))
      .capitalize();
}
