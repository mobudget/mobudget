import 'package:MoBudget/theme/app_theme.dart';
import 'package:flutter/material.dart';

class SituationChip extends StatelessWidget {
  const SituationChip({
    Key key,
    @required this.count,
    @required this.label,
    @required this.color,
  }) : super(key: key);

  final int count;
  final String label;
  final Color color;

  @override
  Widget build(BuildContext context) {
    final ColorScheme colorScheme = Theme.of(context).colorScheme;

    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 2.0),
        child: Chip(
          backgroundColor: color,
          avatar: CircleAvatar(
            backgroundColor: color,
            child: FittedBox(
              child: Text(
                '$count',
                style: TextStyle(
                  color: colorScheme.situationChipColor,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          label: FittedBox(
            child: Text(
              label,
              style: TextStyle(
                color: colorScheme.situationChipColor,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
