import 'package:checkbox_formfield/checkbox_formfield.dart';
import 'package:flutter/material.dart';
import 'package:MoBudget/extensions/double.dart';

class RecordCheckBox extends StatefulWidget {
  const RecordCheckBox({
    Key key,
    @required this.checked,
    @required this.title,
    @required this.value,
    this.onSaved,
  }) : super(key: key);

  final bool checked;
  final String title;
  final double value;
  final FormFieldSetter<bool> onSaved;

  @override
  _RecordCheckBoxState createState() => _RecordCheckBoxState();
}

class _RecordCheckBoxState extends State<RecordCheckBox> {
  @override
  Widget build(BuildContext context) {
    TextTheme textTheme = Theme.of(context).textTheme;
    final String formatedValue = widget.value.toBRL();

    return CheckboxListTileFormField(
      initialValue: widget.checked,
      onSaved: widget.onSaved,
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            widget.title,
            maxLines: 1,
            softWrap: true,
            overflow: TextOverflow.ellipsis,
          ),
          Text(
            formatedValue,
            style: textTheme.bodyText2.copyWith(
              color: textTheme.caption.color,
            ),
          )
        ],
      ),
      controlAffinity: ListTileControlAffinity.leading,
      dense: true,
    );
  }
}
