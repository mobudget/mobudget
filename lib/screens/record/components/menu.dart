import 'package:MoBudget/theme/app_theme.dart';
import 'package:MoBudget/navigation/mobudget_router.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:nuvigator/nuvigator.dart';

class RecordMenu extends StatefulWidget {
  @override
  _RecordMenuState createState() => _RecordMenuState();
}

class _RecordMenuState extends State<RecordMenu> with TickerProviderStateMixin {
  ScrollController scrollController;
  bool dialVisible = true;

  final _reveneuFormKey = GlobalKey<FormState>();
  final _investmentFormKey = GlobalKey<FormState>();
  final _expenseFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();

    scrollController = ScrollController()
      ..addListener(
        () {
          setDialVisible(scrollController.position.userScrollDirection ==
              ScrollDirection.forward);
        },
      );
  }

  void setDialVisible(bool value) {
    setState(() => dialVisible = value);
  }

  @override
  Widget build(BuildContext context) {
    return SpeedDial(
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 22.0),
      visible: dialVisible,
      curve: Curves.bounceInOut,
      overlayColor: Theme.of(context).colorScheme.overlayColor,
      children: _buildMenuItems(context),
    );
  }

  List<SpeedDialChild> _buildMenuItems(BuildContext context) {
    return [
      _buildMenuItem(
        context,
        _expenseFormKey,
        'Adicionar despesa',
        'Despesa',
        Icons.money_off,
      ),
      _buildMenuItem(
        context,
        _investmentFormKey,
        'Adicionar investimento',
        'Investimento',
        Icons.trending_up,
      ),
      _buildMenuItem(
        context,
        _reveneuFormKey,
        'Adicionar receita',
        'Receita',
        Icons.attach_money,
      ),
    ];
  }

  SpeedDialChild _buildMenuItem(
    BuildContext context,
    GlobalKey<FormState> formKey,
    String label,
    String recordTypeLabel,
    IconData icon,
  ) {
    return SpeedDialChild(
      child: Icon(icon),
      label: label,
      labelStyle: TextStyle(fontWeight: FontWeight.w600),
      labelBackgroundColor: Theme.of(context).accentColor,
      onTap: () => Router.of<MoBudgetRouter>(context).toNewRecordRoute(
        recordTypeLabel: recordTypeLabel,
      ),
    );
  }
}
