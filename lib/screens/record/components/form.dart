import 'package:MoBudget/models/base_barcode.dart';
import 'package:MoBudget/navigation/mobudget_router.dart';
import 'package:MoBudget/theme/app_theme.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:nuvigator/nuvigator.dart';

class RecordForm extends StatefulWidget {
  const RecordForm({
    Key key,
    @required this.formKey,
    @required this.recordTypeLabel,
  }) : super(key: key);

  final GlobalKey<FormState> formKey;
  final String recordTypeLabel;

  @override
  _RecordFormState createState() => _RecordFormState();
}

class _RecordFormState extends State<RecordForm> {
  int _category;
  int _situation;
  int _recurrencyDuration = 0;
  bool _isRecurring = false;
  bool _isReservation = false;

  bool _autovalidateName = false;
  bool _autovalidateValue = false;
  bool _autovalidateUsedValue = false;
  bool _autovalidateCategory = false;
  bool _autovalidateSituation = false;

  List<DropdownMenuItem<int>> _items = List();

  void _enableAutovalidateName(String _) =>
      setState(() => _autovalidateName = true);

  TextEditingController _barcodeController = TextEditingController();
  TextEditingController _dateController = TextEditingController();

  MoneyMaskedTextController _valueController =
      MoneyMaskedTextController(leftSymbol: 'R\$ ');
  MoneyMaskedTextController _usedValueController =
      MoneyMaskedTextController(leftSymbol: 'R\$ ');
  MoneyMaskedTextController _expectedValueController =
      MoneyMaskedTextController(leftSymbol: 'R\$ ');

  Flushbar<String> flush;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _newCategoryController = TextEditingController();

  @override
  void initState() {
    _items = _categoryMenuItems();
    super.initState();
  }

  @override
  void dispose() {
    _barcodeController.dispose();
    _dateController.dispose();
    _valueController.dispose();
    _usedValueController.dispose();
    _newCategoryController.dispose();
    super.dispose();
  }

  String _emptyStringValidator(String value) {
    return value.isEmpty ? 'Esse campo não pode ficar em branco' : null;
  }

  String _valueValidator(String _value) {
    return _valueController.numberValue < 0
        ? 'O valor não pode ser negativo'
        : null;
  }

  String _nullValidator(dynamic value) {
    return value == null ? 'Esse campo não pode ficar em branco' : null;
  }

  String _categoryValidator(dynamic value) {
    return [null, 15].contains(value) ? 'Selecione uma categoria' : null;
  }

  Color _situationIconColor(BuildContext context) {
    final colorScheme = Theme.of(context).colorScheme;

    switch (_situation) {
      case 1:
        return colorScheme.confirmedColor;
        break;
      case 2:
        return colorScheme.pendingColor;
        break;
      case 3:
        return colorScheme.scheduledColor;
      default:
        return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: widget.formKey,
      child: Column(
        children: <Widget>[
          TextFormField(
            validator: _emptyStringValidator,
            autovalidate: _autovalidateName,
            onChanged: _enableAutovalidateName,
            decoration: InputDecoration(
              icon: Icon(Icons.short_text),
              hintText: widget.recordTypeLabel,
            ),
            autofocus: true,
          ),
          TextFormField(
            minLines: 1,
            maxLines: 3,
            maxLength: 100,
            decoration: InputDecoration(
              icon: Icon(Icons.subject),
              labelText: 'Descrição',
            ),
          ),
          Visibility(
            visible: widget.recordTypeLabel == 'Despesa',
            child: TextFormField(
              controller: _barcodeController,
              keyboardType: TextInputType.number,
              minLines: 1,
              maxLines: 2,
              maxLength: 48,
              decoration: InputDecoration(
                icon: FaIcon(FontAwesomeIcons.barcode),
                labelText: 'Código de barras',
                suffixIcon: IconButton(
                  icon: FaIcon(FontAwesomeIcons.camera),
                  onPressed: () async {
                    final BaseBarcode barcode =
                        await Router.of<MoBudgetRouter>(context)
                            .toBarcodeScanPage();
                    _barcodeController.text = barcode.readableBarcode();
                    _valueController.text = barcode.value().toString();
                    _dateController.text = barcode.dueDate();
                  },
                ),
              ),
            ),
          ),
          CheckboxListTile(
            secondary: FaIcon(FontAwesomeIcons.coins),
            value: _isReservation,
            onChanged: (value) => setState(() {
              FocusScope.of(context).unfocus();
              _isReservation = value;
            }),
            title: Text('É uma reserva?'),
            subtitle: Text('O valor ainda não foi totalmente utilizado?'),
            dense: true,
            contentPadding: EdgeInsets.all(0.0),
          ),
          TextFormField(
            controller: _valueController,
            keyboardType: TextInputType.number,
            validator: _valueValidator,
            autovalidate: _autovalidateValue,
            onChanged: (value) {
              setState(() => _autovalidateValue = true);
            },
            decoration: InputDecoration(
              icon: _isReservation
                  ? FaIcon(FontAwesomeIcons.moneyCheckAlt)
                  : Icon(Icons.attach_money),
              labelText: _isReservation ? 'Valor reservado' : 'Valor',
            ),
          ),
          Visibility(
            visible: _isReservation,
            child: TextFormField(
              controller: _usedValueController,
              keyboardType: TextInputType.number,
              validator: _valueValidator,
              autovalidate: _autovalidateUsedValue,
              onChanged: (value) {
                setState(() => _autovalidateUsedValue = true);
              },
              decoration: InputDecoration(
                icon: Icon(Icons.attach_money),
                labelText: 'Valor já utilizado',
              ),
            ),
          ),
          DateTimeField(
            controller: _dateController,
            decoration: InputDecoration(
              icon: Icon(Icons.today),
              labelText: 'Data',
            ),
            validator: _nullValidator,
            autovalidate: true,
            format: DateFormat("dd/MM/yyyy"),
            initialValue: DateTime.now(),
            onShowPicker: (context, currentValue) {
              return showDatePicker(
                context: context,
                firstDate: DateTime(1900),
                initialDate: currentValue ?? DateTime.now(),
                lastDate: DateTime(2100),
              );
            },
          ),
          DropdownButtonFormField(
            value: _category,
            onTap: () => FocusScope.of(context).unfocus(),
            autovalidate: _autovalidateCategory,
            decoration: InputDecoration(
              icon: Icon(Icons.bookmark),
              labelText: 'Categoria',
            ),
            items: _items,
            onChanged: (value) {
              if (value == 15) {
                flush = Flushbar<String>(
                  userInputForm: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        TextFormField(
                          controller: _newCategoryController,
                          style: TextStyle(color: Colors.white),
                          maxLength: 100,
                          maxLines: 1,
                          maxLengthEnforced: true,
                          autofocus: true,
                          decoration: InputDecoration(
                            fillColor: Colors.white10,
                            filled: true,
                            icon: Icon(Icons.bookmark, color: Colors.grey[500]),
                            border: UnderlineInputBorder(),
                            helperStyle: TextStyle(color: Colors.grey),
                            labelText: "Nova Categoria",
                            labelStyle: TextStyle(color: Colors.grey),
                          ),
                          validator: _emptyStringValidator,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            MaterialButton(
                              textColor: Theme.of(context).primaryColor,
                              child: Text("Cancelar"),
                              onPressed: () {
                                flush.dismiss();
                              },
                            ),
                            MaterialButton(
                              textColor: Theme.of(context).primaryColor,
                              child: Text("Adicionar"),
                              onPressed: () {
                                if (_formKey.currentState.validate()) {
                                  flush.dismiss(_newCategoryController.text);
                                }
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
                flush.show(context).then(
                  (result) {
                    if (result != null && result.isNotEmpty) {
                      List newItems = _categoryMenuItemsNew(result);

                      setState(() {
                        _category = newItems.last.value;
                        _items = newItems;
                      });
                    }
                    setState(() => _autovalidateCategory = true);
                    _newCategoryController.clear();
                  },
                );
              } else {
                setState(() => _category = value);
              }
            },
            validator: _categoryValidator,
          ),
          DropdownButtonFormField(
            value: _situation,
            onTap: () => FocusScope.of(context).unfocus(),
            autovalidate: _autovalidateSituation,
            decoration: InputDecoration(
              icon: Icon(
                Icons.lens,
                color: _situationIconColor(context),
              ),
              labelText: 'Situação',
            ),
            items: _situationMenuItems(),
            onChanged: (value) => setState(() {
              _situation = value;
              _autovalidateSituation = true;
            }),
            validator: _nullValidator,
          ),
          CheckboxListTile(
            secondary: FaIcon(FontAwesomeIcons.syncAlt),
            value: _isRecurring,
            onChanged: (value) => setState(() {
              FocusScope.of(context).unfocus();
              _isRecurring = value;
              _expectedValueController.value = _valueController.value;
            }),
            title: Text('Recorrente?'),
            subtitle: Text('Deve se repetir nos próximos meses?'),
            dense: true,
            contentPadding: EdgeInsets.all(0.0),
          ),
          Visibility(
              visible: _isRecurring,
              child: Column(
                children: [
                  DropdownButtonFormField(
                    value: _recurrencyDuration,
                    decoration: InputDecoration(
                      icon: FaIcon(FontAwesomeIcons.clock),
                      labelText: 'Duração da recorrência',
                    ),
                    items: List<DropdownMenuItem>.generate(
                      25,
                      (index) => DropdownMenuItem(
                        child: Text(recurrencyMenuItemLabel(index)),
                        value: index,
                      ),
                    ),
                    onChanged: (value) => setState(() {
                      _recurrencyDuration = value;
                    }),
                    validator: _nullValidator,
                  ),
                  TextFormField(
                    controller: _expectedValueController,
                    keyboardType: TextInputType.number,
                    validator: _valueValidator,
                    autovalidate: _autovalidateValue,
                    onChanged: (value) {
                      setState(() => _autovalidateValue = true);
                    },
                    decoration: InputDecoration(
                      icon: Icon(Icons.attach_money),
                      labelText: 'Valor recorrente',
                    ),
                  ),
                ],
              ))
        ],
      ),
    );
  }

  recurrencyMenuItemLabel(int value) {
    return Intl.plural(
      value,
      zero: 'Indeterminada',
      one: '$value mês',
      other: '$value meses',
      name: "DropdownButtonFormField items",
      args: [value],
      examples: const {'value': 0},
      desc: "DropdownButtonFormField values",
    );
  }

  List<DropdownMenuItem<int>> _situationMenuItems() {
    return [
      DropdownMenuItem(
        child: Text('Confirmada'),
        value: 1,
      ),
      DropdownMenuItem(
        child: Text('Pendente'),
        value: 2,
      ),
      DropdownMenuItem(
        child: Text('Agendada'),
        value: 3,
      ),
    ];
  }

  List<DropdownMenuItem<int>> _categoryMenuItems() {
    return [
      DropdownMenuItem(
        child: Text('Alimentação'),
        value: 1,
      ),
      DropdownMenuItem(
        child: Text('Cartão de crédito'),
        value: 2,
      ),
      DropdownMenuItem(
        child: Text('Casa'),
        value: 3,
      ),
      DropdownMenuItem(
        child: Text('Dívidas'),
        value: 4,
      ),
      DropdownMenuItem(
        child: Text('Educação'),
        value: 5,
      ),
      DropdownMenuItem(
        child: Text('Eletrônicos'),
        value: 6,
      ),
      DropdownMenuItem(
        child: Text('Filhos'),
        value: 7,
      ),
      DropdownMenuItem(
        child: Text('Financiamentos'),
        value: 8,
      ),
      DropdownMenuItem(
        child: Text('Lazer'),
        value: 9,
      ),
      DropdownMenuItem(
        child: Text('Roupas'),
        value: 10,
      ),
      DropdownMenuItem(
        child: Text('Saúde'),
        value: 11,
      ),
      DropdownMenuItem(
        child: Text('Transporte'),
        value: 12,
      ),
      DropdownMenuItem(
        child: Text('Viagem'),
        value: 13,
      ),
      DropdownMenuItem(
        child: Text('Outros'),
        value: 14,
      ),
      DropdownMenuItem(
        child: Text('Adicionar categoria ...'),
        value: 15,
      ),
    ];
  }

  List<DropdownMenuItem<int>> _categoryMenuItemsNew(String newCategory) {
    List<DropdownMenuItem<int>> items;
    items = _items;
    items.add(
      DropdownMenuItem<int>(
        child: Text(newCategory),
        value: _items.last.value + 1,
      ),
    );
    return items;
  }
}
