import 'package:MoBudget/components/screen.dart';
import 'package:MoBudget/theme/app_theme.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';

import 'components/form.dart';

class NewRecordScreen extends StatefulWidget {
  const NewRecordScreen({
    Key key,
    @required this.recordTypeLabel,
  }) : super(key: key);

  final String recordTypeLabel;

  @override
  _NewRecordScreenState createState() => _NewRecordScreenState();
}

class _NewRecordScreenState extends State<NewRecordScreen> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final ColorScheme colorScheme = theme.colorScheme;

    return MoBudgetScreen(
      title: 'Adicionando...',
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                RecordForm(
                  formKey: _formKey,
                  recordTypeLabel: widget.recordTypeLabel,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: Container(
                    margin: EdgeInsets.all(4.0),
                    width: double.infinity,
                    child: RaisedButton(
                      child: Text('Salvar'),
                      textColor: Colors.white,
                      color: Theme.of(context).accentColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                      ),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          Navigator.pop(context);

                          Flushbar(
                            icon: Icon(
                              Icons.check_circle,
                              size: 28.0,
                              color: colorScheme.flushbarIconColor,
                            ),
                            message: 'Salvo com sucesso!',
                            duration: Duration(seconds: 3),
                            leftBarIndicatorColor:
                                colorScheme.flushbarIndicatorColor,
                            dismissDirection:
                                FlushbarDismissDirection.HORIZONTAL,
                          )..show(context);
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
