import 'package:MoBudget/models/base_barcode.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:flutter_camera_ml_vision/flutter_camera_ml_vision.dart';

import 'components/overlay_shape.dart';

class BarcodeScanScreen extends StatefulWidget {
  @override
  _BarcodeScanScreenState createState() => _BarcodeScanScreenState();
}

class _BarcodeScanScreenState extends State<BarcodeScanScreen> {
  bool resultSent = false;
  BarcodeDetector detector = FirebaseVision.instance.barcodeDetector(
    BarcodeDetectorOptions(barcodeFormats: BarcodeFormat.itf),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Centralize o boleto na tela'), elevation: 0),
      body: SafeArea(
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: CameraMlVision<List<Barcode>>(
            overlayBuilder: (c) {
              return Container(
                decoration: ShapeDecoration(
                  shape: BarcodeScanOverlayShape(
                    borderColor: Theme.of(context).primaryColor,
                    borderWidth: 3.0,
                  ),
                ),
              );
            },
            detector: detector.detectInImage,
            onResult: (List<Barcode> barcodes) {
              if (!mounted || resultSent || invaliBarcode(barcodes)) return;

              resultSent = true;
              BaseBarcode barcode = BaseBarcode.build(barcodes.first.rawValue);
              Navigator.of(context).pop<BaseBarcode>(barcode);
            },
            onDispose: () => detector.close(),
          ),
        ),
      ),
    );
  }

  bool invaliBarcode(List<Barcode> barcodes) {
    return barcodes == null ||
        barcodes.isEmpty ||
        barcodes.first.rawValue.length != 44;
  }
}
