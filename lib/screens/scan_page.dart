import 'package:MoBudget/models/base_barcode.dart';
import 'package:MoBudget/navigation/mobudget_router.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nuvigator/nuvigator.dart';
import 'package:url_launcher/url_launcher.dart';

import 'barcode_scan/screen.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({
    Key key,
  }) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<String> data = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Scan Page'),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          RaisedButton(
            child: Text('Scan product'),
            onPressed: () async {
              final BaseBarcode barcode =
                  await Router.of<MoBudgetRouter>(context).toBarcodeScanPage();

              if (barcode == null) return;

              setState(() {
                data = [];
                data.add(barcode.toString());
                data.add(barcode.readableBarcode());
                data.add('Value: ${barcode.value()}');
                data.add('DueDate: ${barcode.dueDate()}');
                data.add(
                    'isUtilities ${BaseBarcode.isUtitilies(barcode.barcode)}');
              });

              // Clipboard.setData(
              //   ClipboardData(
              //     text: barcode.readableBarcode(),
              //   ),
              // ).then(
              //   (_) async {
              //     await launch(
              //       'https://nuapp.nubank.com.br/bnVhcHA6Ly9zYXZpbmdz',
              //       forceSafariVC: false,
              //       universalLinksOnly: true,
              //     );
              //   },
              // );
            },
          ),
          Expanded(
            child: ListView(
              children: data.map((d) => Text(d)).toList(),
            ),
          ),
        ],
      ),
    );
  }
}
