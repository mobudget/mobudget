import 'package:flutter/material.dart';

class MoBudgetScreen extends StatelessWidget {
  const MoBudgetScreen({
    Key key,
    this.title = 'MoBudget',
    this.topSheet,
    @required this.body,
    this.bottomSheet,
    this.floatingActionButton,
  }) : super(key: key);

  final String title;
  final Widget topSheet;
  final Widget body;
  final Widget bottomSheet;
  final Widget floatingActionButton;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        elevation: 0,
        bottom: topSheet,
      ),
      body: SingleChildScrollView(
        child: body,
      ),
      floatingActionButton: floatingActionButton,
      bottomNavigationBar: bottomSheet,
    );
  }
}
