import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

class ExpandableCard extends StatelessWidget {
  const ExpandableCard({
    Key key,
    @required this.header,
    this.collapsed,
    @required this.expanded,
    this.tapBodyToCollapse = true,
    this.tapBodyToExpand = true,
    this.hasIcon = true,
    this.padding,
  }) : super(key: key);

  final Widget header;
  final Widget collapsed;
  final Widget expanded;
  final bool tapBodyToCollapse;
  final bool tapBodyToExpand;
  final bool hasIcon;
  final EdgeInsets padding;

  @override
  Widget build(BuildContext context) {
    return ExpandableNotifier(
      child: Padding(
        padding: padding ?? const EdgeInsets.all(0.0),
        child: Card(
          elevation: 4,
          clipBehavior: Clip.antiAlias,
          child: Column(
            children: <Widget>[
              ScrollOnExpand(
                scrollOnExpand: true,
                scrollOnCollapse: true,
                child: ExpandablePanel(
                  theme: ExpandableThemeData(
                    headerAlignment: ExpandablePanelHeaderAlignment.center,
                    hasIcon: hasIcon,
                    tapBodyToCollapse: tapBodyToCollapse,
                    tapBodyToExpand: tapBodyToExpand,
                    crossFadePoint: 0.0,
                  ),
                  header: header,
                  collapsed: collapsed,
                  expanded: expanded,
                  builder: (_, collapsed, expanded) {
                    return Padding(
                      padding: EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
                      child: Expandable(
                        collapsed: collapsed,
                        expanded: expanded,
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
