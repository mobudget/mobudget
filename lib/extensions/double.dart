import 'package:intl/intl.dart';

extension DoubleExtension on double {
  String toBRL() {
    return NumberFormat.currency(locale: Intl.getCurrentLocale(), symbol: 'R\$')
        .format(this);
  }
}
