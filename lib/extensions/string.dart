extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1).toLowerCase()}";
  }

  int toInteger() {
    return int.tryParse(this);
  }

  double toMoney() {
    return double.tryParse(this) / 100;
  }
}
